# LightZymeOne

LightZyme1.0 photo reactor project, developed in the Matthias Hoehne Group Uni-Greifswald.

This photoreactor has the following features:

* 4 independant channels with light intensity and temperature control
* 4 stirring units
* Arduino based control loops
* Raspberry Pi as SiLA2 Server and for the Graphical User Interface
* Graphical User Interface written in labPyUi

 